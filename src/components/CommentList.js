import React from 'react';
import { ListItem } from '@mui/material';
import PropTypes from 'prop-types';

const CommentItem = ({comment}) => {
	return (
		<>
			<ListItem key={comment.id}>
				<h5>{comment.comment}</h5>
				<hr />
				<h6>{comment.author}</h6>
			</ListItem>
			<hr />
		</>
	);
};

const CommentList = (props) => {
	const { commentList } = props;

	const mappedComments = commentList.map(comment => {
		return (
			<CommentItem key={comment.id} comment={comment}>
			</CommentItem>
		);
	});  

	return (
		<>
			{mappedComments}
		</>
	);
};

CommentItem.propTypes = {
	comment: PropTypes.object
};

CommentList.propTypes = {
	commentList: PropTypes.array
};

export default CommentList;