import WidgetBot from '@widgetbot/react-embed';
import React from 'react';

const Demo = () => {

	return (
		<WidgetBot
			server="927490485772230686"
			channel="927490485772230689"
			width="80%"
			height="80vh"
			margin="10px"
		/>
	);
};

export default Demo;