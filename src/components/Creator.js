import React from 'react';

const Creator = () => {
	return (
		<main role="main" className="container">
			<div className="row">
				<div className="col-md-8 blog-main">
					<h2 className="pb-3 mb-4 font-italic border-bottom text-white">
				About the creator
					</h2>
					<div className="blog-post">
						<h2 className="blog-post-title text-white">Who is Jay?</h2>
						<hr />
						<blockquote>
							<p className="text-white">Long version about me is that ever since I was little, my dad and uncle were my biggest influencers when it came to computers and video games.</p>
						</blockquote>
						<p className="text-white">Growing up I played WoW on my mom&apos;s account and sometimes on my dad&apos;s account as well. That is when I started getting into video games and I got interested in how they were made. Eventually I got into coding as well later in life and I have been doing it as a hobby ever since.</p>
						<p className="text-white">I always had a heart for animals as well and I did study to be an animal care attendant but I never made a career out of it.</p>
					</div>
				</div>
				<aside className="col-md-4 blog-sidebar">
					<div className="p-3 mb-3 bg-light rounded">
						<h4 className="font-italic">Shortly</h4>
						<p className="mb-0">I am a 23-year-old woman from Finland, Tampere and I have a cat named M&apos;aiq.</p>
					</div>
					<div className="p-3 bg-light rounded">
						<h4 className="font-italic">Elsewhere</h4>
						<ol className="list-unstyled">
							<li><a href="https://github.com/JayPeura/">GitHub</a></li>
							<li><a href="https://twitter.com/FelineJay/">Twitter</a></li>
							<li><a href="https://www.linkedin.com/in/jay-peura-0ba446185/">LinkedIn</a></li>
						</ol>
					</div>
				</aside>
			</div>
		</main>
	);
};

export default Creator;
