import React, { useState, useEffect } from 'react';
import CommentList from './CommentList';


const CommentContainer = () => {

	const [commentList, setCommentList] = useState([]);

	useEffect(() => {
		const getComment = async () => {
			const result = await fetch('http://localhost:3004/comments', {
				method: 'GET',
				headers: {'Content-Type': 'application/json'}
			});
			if(result.status === 200 || result.status === 304) {
				const content = await result.json();
				setCommentList(content);
			} else {
				alert('Something went wrong: status ' + result.status);
			}
		};
		getComment();
	}, []);

	return (
		<>
			<CommentList commentList={commentList} />
		</>

	);
};

export default CommentContainer;