import React from 'react';
import { Link } from 'react-router-dom';


const NavBar = () => {
	return (
		<nav className="navbar navbar-expand-md navbar-dark bg-dark mb-4">
			<div className="container-fluid">
				<a className="navbar-brand" href="/">Home</a>
				<ul className="navbar-nav me-auto mb-2 mb-md-0">
					<li className="nav-item">
						<Link to="/documentation" className="nav-link active">Documentation</Link>
					</li>
					<li className="nav-item">
						<Link to="/creator" className="nav-link active">Creator</Link>
					</li>
					<li className="nav-item">
						<Link to="/demo" className="nav-link active">Demo</Link>
					</li>
				</ul>
			</div>
		</nav>
	);
};

export default NavBar;
